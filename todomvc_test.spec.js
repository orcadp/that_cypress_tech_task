/// <reference types="cypress" />

//I am pretty new to this tool, so no Page Object or something even 
//fancier here:) also no code reusability due to lack of time
context('ToDoMvc', () => {
    beforeEach(() => {
      cy.visit('http://todomvc.com/examples/angular2/')
    })
    const todo_text = 'testToDo';
    const udpated_todo_text = 'testToDoUPD';

    const count_locator = '.todo-count > strong';
    const todo_list_label_locator = '.view > label';
    const enter_todo_locator = '.header > input';
    const todo_list_locator = '.todo-list > li';
    const todo_checkbox_locator = '.todo-list .toggle';
    const destroy_todo_button = '.destroy';

    var todos_current_count;

    it('add todo', () => {
        cy.get(enter_todo_locator)
          .type(todo_text)
          .should('have.value', 'testToDo')
          .type('{enter}')
          .get('.todo-list')
          .find('li')
          .get(todo_list_label_locator)
          .should('have.text', todo_text)
          .get(count_locator)
          .then(($lbl) => {
            todos_current_count = parseFloat($lbl.text());
        })
          .invoke('text')
          .as('todo_count')
          .then(parseFloat)
          .should('be.gt', 0);
        })
    it('update todo',() => {
        cy.get(enter_todo_locator)
          .type(todo_text)
          .type('{enter}')
          .get(todo_list_locator)
          .dblclick()
          .get('.edit')
          .clear()
          .type(udpated_todo_text)
          .type('{enter}')
          .get(todo_list_label_locator)
          .should('have.text', udpated_todo_text);
        
        cy.get(count_locator)
          .invoke('text')
          .then(parseFloat)
          .should('be.eq', todos_current_count);
        })

    it('execute todo',() => {
        cy.get(enter_todo_locator)
          .type(todo_text)
          .type('{enter}')
          .get(todo_checkbox_locator)
          .click()
          .get('.todo-list .completed')
          .should('be.visible')
          .get(count_locator)
          .invoke('text')
          .then(parseFloat)
          .should('be.eq', 0);
        })

    it('delete todo',() => {
        cy.get(enter_todo_locator)
          .type(todo_text)
          .type('{enter}')
          .get(destroy_todo_button)
          .click({ force: true })
          .get(todo_list_label_locator)
          .should('not.exist')
          .get(count_locator)
          .should('not.exist');
        })

    it('negative: add already existing todo',() => {
        cy.get(enter_todo_locator)
          .type(todo_text)
          .type('{enter}')
          .get(enter_todo_locator)
          .type(todo_text)
          .type('{enter}')
          .get('.todo-list')
          .find('li')
          .should('have.length', 1); //check fails 
                                        //due to bug here as it is possible to add duplicate todo
        })

    it('negative: add todo that has been executed already',() => {
        cy.get(enter_todo_locator)
          .type(todo_text)
          .type('{enter}')
          .get(todo_checkbox_locator)
          .click()
          .get(enter_todo_locator)
          .type(todo_text)
          .type('{enter}')
          .get('.todo-list')
          .find('li')
          .should('have.length', 1); //check fails 
                                        //due to bug here as it is possible to add duplicate todo
        })
    })
    
    