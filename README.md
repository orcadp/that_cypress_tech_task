System requirements: 
Make sure your system is compliant to https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements

Preconditions: 
1. setup Node.js
-  Windows: 
	1) download Node.js installer executable for Windows from https://nodejs.org/en/download/ and install
	2) verify installation with command (PowerShell or CMD)
		node –v
	or
		npm –v
Linux: 
	1) open your terminal or press Ctrl + Alt + T
	2) execute command: 
		sudo apt install nodejs
	3) verify installation with command
		node -v 
	or 
		node –version
MacOS:
	1) download Node.js installer executable for MacOS from https://nodejs.org/en/download/ and install
	2) verify installation with command (terminal)
		node –v
2. create Cypress-Automation folder in any location on your machine where you have admin rights; perform all the following steps from that folder
3. setup Cypress 
- Windows:
	1) set the NODE_HOME in the environment variable which contains full path to nodeJs folder (e.g. C:\Program Files\nodejs)
	
	2) execute in PowerShell or CMD: 
		npm-init
		npm install cypress --save-dev 
- MacOS and Linux:
	1) execute in Terminal: 
		npm-init
		npm install cypress --save-dev 
4. create Cypress-specific folders (all platforms): 
	1) npx cypress open
5. copy spec file (todomvc_test.spec.js) to /integration folder
6. make sure you have cypress.json file in /integration folder with following json entity: 
	  {"integrationFolder": “.",
		"testFiles": "**.spec.js"}


Running tests:  
(all platforms) execute from /Cypress-Automation/integration folder:
	$(npm bin)/cypress run -s todomvc_test.spec.js

